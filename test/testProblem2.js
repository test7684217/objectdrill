const userData = require("../1-users_1.cjs");
const problem2 = require("../problem2");

const usersStayingInGermany = problem2(userData);

if (usersStayingInGermany.length>0) {
  console.log("Users staying in Germany: ");

  for (let index = 0; index < usersStayingInGermany.length; index++) {
    console.log(usersStayingInGermany[index]);
  }
}
