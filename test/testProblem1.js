
const userData = require('../1-users_1.cjs');
const problem1 = require('../problem1');


const usersInterestedInVideoGames = problem1(userData);

if(usersInterestedInVideoGames.length){
    console.log("Users interested in playing video games: ");
    for (let index = 0; index < usersInterestedInVideoGames.length; index++) {
        console.log(usersInterestedInVideoGames[index])
    }   
}