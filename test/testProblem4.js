const userData = require("../1-users_1.cjs");
const problem4 = require("../problem4");

const usersGroupedByProgrammingLanguages = problem4(userData);

console.log("Users Grouped By Programming Languages:");
console.log(usersGroupedByProgrammingLanguages);
