const userData = require("../1-users_1.cjs");
const problem3 = require("../problem3");

const usersWithMastersDegree = problem3(userData);

if (usersWithMastersDegree.length>0) {
  console.log("Users with Masters Degree: ");

  for (let index = 0; index < usersWithMastersDegree.length; index++) {
    console.log(usersWithMastersDegree[index]);
  }
}
