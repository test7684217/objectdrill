function problem4(users) {
  try {
    if (typeof users !== "object" || users === null) {
      throw new Error("Users data input should be an object");
    }

    let usersGroupedByProgrammingLanguages = {
      Golang: [],
      JavaScript: [],
      Python: [],
    };

    let languages = Object.keys(usersGroupedByProgrammingLanguages);

    for (const key in users) {
      if(users[key].desgination){

        let userDesignation = users[key].desgination;

        for (let index = 0; index < languages.length; index++) {
            if (userDesignation && userDesignation.toLowerCase().includes(languages[index].toLowerCase())) 
            {
                usersGroupedByProgrammingLanguages[languages[index]].push(
                    key
                );
            }
        }
      }
    }

    return usersGroupedByProgrammingLanguages;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}

module.exports = problem4;
