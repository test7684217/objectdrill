function problem2(users) {
  try {

    if(typeof users!=="object" || users===null)
    {
        throw new Error("Users data input should be an object")
    }

    let usersStayingInGermany = [];

    for (const key in users) {
      let userNationality = users[key].nationality;
      
      if (userNationality && userNationality.toLowerCase() === "germany") {
        usersStayingInGermany.push(key);
      }
    }

    if(usersStayingInGermany.length==0){
        console.log("No users staying in germany")
    }

    return usersStayingInGermany;

  } catch (error) {

    console.log(error.message);
    return [];
  }
}

module.exports = problem2;
