function problem1(users) {
  try {

    if(typeof users!=="object" || users===null)
    {
        throw new Error("Users data input should be an object")
    }

    let usersInterestedInVideoGames = [];

    for (const key in users) {
      let userInterests = users[key].interests;

      if (userInterests) {
        for (let index = 0; index < userInterests.length; index++) {
          if (userInterests[index].toLowerCase().includes("video games")) {
            usersInterestedInVideoGames.push(key);
          }
        }
      }
    }

    if(usersInterestedInVideoGames.length==0){
        console.log("No user is interested in Playing Videos Games");
    }

    return usersInterestedInVideoGames;

  } catch (error) {
    console.log(error.message);
    return [];
  }
}

module.exports = problem1;
