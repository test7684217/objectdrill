function problem3(users) {
  try {
    if (typeof users !== "object" || users === null) {
      throw new Error("Users data input should be an object");
    }

    let usersWithMastersDegree = [];

    for (const key in users) {
      let userQualifications = users[key].qualification;

      if (userQualifications && userQualifications.toLowerCase() === "masters") {
        usersWithMastersDegree.push(key);
      }
    }

    return usersWithMastersDegree;

  } catch (error) {
    console.log(error.message);
    return [];
  }
}

module.exports = problem3;
